package nl.bioinf.fp;

public final class Gene {
    private static final Coordinate DEFAULT_COORDINATE;
    static {
        DEFAULT_COORDINATE = new Coordinate(0, 0, false);
    }
    private Coordinate coordinate;
    private String name;
    private GoAnnotation goAnnotation;

    public Gene(Coordinate coordinate, String name, GoAnnotation goAnnotation) {
        if (coordinate == null) {
            this.coordinate = DEFAULT_COORDINATE;
        } else {
            this.coordinate = coordinate;
        }

        this.name = name;
        this.goAnnotation = goAnnotation;
    }


    public Coordinate getCoordinate() {
        return coordinate;
    }

    public Gene setCoordinate(Coordinate newCoordinate) {
//        this.coordinate = newCoordinate;
        return null;
    }

    public String getName() {
        return name;
    }

    public GoAnnotation getGoAnnotation() {
        return goAnnotation;
    }


}
