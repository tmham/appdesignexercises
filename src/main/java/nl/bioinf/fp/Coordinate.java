package nl.bioinf.fp;

public class Coordinate {
    private int start;
    private int end;
    private boolean onComplement;

    public Coordinate(int start, int end, boolean onComplement) {
        this.start = start;
        this.end = end;
        this.onComplement = onComplement;
    }
}
