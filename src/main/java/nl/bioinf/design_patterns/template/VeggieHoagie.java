package nl.bioinf.design_patterns.template;

public class VeggieHoagie extends Hoagie{

    String[] veggiesUsed = {"Lettuce", "Tomatoes"};
    String[] condimentsUsed = {"Oil", "Vinegar"};

    boolean customerWantsMeat() {return false; }
    boolean customerWantsCheese() {return false; }

    @Override
    void addMeat() {
    }

    @Override
    void addCheese() {
    }

    @Override
    void addVegetables() {
        System.out.println("Adding the veggies");
        for(String veggie : veggiesUsed) {
            System.out.println(veggie + " ");
        }
    }

    @Override
    void addCondiments() {
        System.out.println("Adding the condiments");
        for(String condiment : condimentsUsed) {
            System.out.println(condiment + " ");
        }
    }
}