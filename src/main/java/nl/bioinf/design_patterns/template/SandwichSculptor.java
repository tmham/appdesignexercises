package nl.bioinf.design_patterns.template;

public class SandwichSculptor {
    public static void main(String[] args) {
        Hoagie customer1Hoagie = new ItalianHoagie();

        customer1Hoagie.makeSandwich();

        Hoagie customer2Hoagie = new VeggieHoagie();
        customer2Hoagie.makeSandwich();
    }
}
