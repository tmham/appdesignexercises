package nl.bioinf.design_patterns.template;

public class ItalianHoagie extends Hoagie{

    String[] meatUsed = {"Salami", "Pepperonie"};
    String[] cheeseUsed = {"Provolone"};
    String[] veggiesUsed = {"Lettuce", "Tomatoes"};
    String[] condimentsUsed = {"Oil", "Vinegar"};

    @Override
    void addMeat() {
        System.out.println("Adding the meat");
        for(String meat : meatUsed) {
            System.out.println(meat + " ");
        }
    }

    @Override
    void addCheese() {
        System.out.println("Adding the cheese");
        for(String cheese : cheeseUsed) {
            System.out.println(cheese + " ");
        }
    }

    @Override
    void addVegetables() {
        System.out.println("Adding the veggies");
        for(String veggie : veggiesUsed) {
            System.out.println(veggie + " ");
        }

    }

    @Override
    void addCondiments() {
        System.out.println("Adding the condiments");
        for(String condiment : condimentsUsed) {
            System.out.println(condiment + " ");
        }
    }
}
