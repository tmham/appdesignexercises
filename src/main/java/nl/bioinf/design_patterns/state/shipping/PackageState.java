package nl.bioinf.design_patterns.state.shipping;

public interface PackageState {
    void next(Package pkg);
    void prev(Package pkg);
    void printStatus();
}
