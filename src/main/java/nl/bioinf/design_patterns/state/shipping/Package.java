package nl.bioinf.design_patterns.state.shipping;

public class Package {
    private PackageState packageState;

    public PackageState getPackageState() {
        return packageState;
    }

    public void setPackageState(PackageState packageState) {
        this.packageState = packageState;
    }

    public void previousState() {
        packageState.prev(this);
    }

    public void nextState() {
        packageState.next(this);
    }

    public void printStatus() {
        packageState.printStatus();
    }
}
