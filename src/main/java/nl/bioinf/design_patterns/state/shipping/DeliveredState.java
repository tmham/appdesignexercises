package nl.bioinf.design_patterns.state.shipping;

public class DeliveredState implements PackageState {

    @Override
    public void next(Package pkg) {
        pkg.setPackageState(new ReceivedState());

    }

    @Override
    public void prev(Package pkg) {
        pkg.setPackageState(new RequestedState());
    }

    @Override
    public void printStatus() {
        System.out.println("Packaged is to be delivered.");

    }
}
