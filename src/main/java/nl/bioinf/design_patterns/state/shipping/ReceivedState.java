package nl.bioinf.design_patterns.state.shipping;

public class ReceivedState implements PackageState {
    @Override
    public void next(Package pkg) {
        System.out.println("There is no next state.");
    }

    @Override
    public void prev(Package pkg) {
        pkg.setPackageState(new DeliveredState());
    }

    @Override
    public void printStatus() {
        System.out.println("package is delivered.");
    }
}
