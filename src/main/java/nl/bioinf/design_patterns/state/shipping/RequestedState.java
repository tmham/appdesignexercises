package nl.bioinf.design_patterns.state.shipping;

public class RequestedState implements PackageState {


    @Override
    public void next(Package pkg) {
        pkg.setPackageState(new DeliveredState());
    }

    @Override
    public void prev(Package pkg) {
        System.out.println("Package is in root state");
    }

    @Override
    public void printStatus() {
        System.out.println("Packaged is Ordered.");
    }
}
