package nl.bioinf.design_patterns.state.musicplayer;

public class ReadyState extends State {

    ReadyState(Player player) {
        super(player);
    }

    @Override
    public String onLock() {
        player.changeState(new LockedState(player));
        return "Locked...";
    }

    @Override
    public String onPlay() {
        String action = player.startPlayback();
        player.changeState(new PlayingState(player));
        return action;
    }

    @Override
    public String onNext() {
        return "locked";
    }

    @Override
    public String onPrevious() {
        return "locked";
    }
}
