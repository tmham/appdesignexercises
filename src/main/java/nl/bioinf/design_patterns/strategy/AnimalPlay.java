package nl.bioinf.design_patterns.strategy;

public class AnimalPlay {

    public static void main(String[] args) {
        Animal sparky = new Dog();

        Animal tweety = new Bird();

        System.out.println("Bird: " + tweety.tryToFly());
        System.out.println("Dog: " + sparky.tryToFly());

        sparky.setFlyingType(new ItFlys());
        System.out.println("Dog: " + sparky.tryToFly());
    }
}
