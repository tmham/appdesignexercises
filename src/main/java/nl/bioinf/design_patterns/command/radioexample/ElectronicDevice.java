package nl.bioinf.design_patterns.command.radioexample;

public interface ElectronicDevice {
    void turnOn();
    void turnOff();
    void pauze();
    void play();
    void lowerVolume();
    void higherVolume();
}
