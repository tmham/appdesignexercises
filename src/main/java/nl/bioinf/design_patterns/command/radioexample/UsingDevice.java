package nl.bioinf.design_patterns.command.radioexample;

public class UsingDevice {
    public static void main(String[] args) {
        Radio myRadio = new Radio();

        TurnRadioOn onCommand = new TurnRadioOn(myRadio);
        UseDevice pressedOn = new UseDevice(onCommand);
        pressedOn.pressButton();
    }
}
