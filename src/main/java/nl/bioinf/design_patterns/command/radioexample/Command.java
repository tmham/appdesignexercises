package nl.bioinf.design_patterns.command.radioexample;

public interface Command {
    void execute();
    void undo();
}
