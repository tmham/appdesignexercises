package nl.bioinf.design_patterns.command.radioexample;

public class UseDevice {
    Command theCommand;

    public  UseDevice(Command newCommand) {
        theCommand = newCommand;
    }
    public void pressButton() {
        theCommand.execute();
    }
}
