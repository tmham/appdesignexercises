package nl.bioinf.design_patterns.command.radioexample;

public class Radio implements ElectronicDevice {

    private int volume;

    @Override
    public void turnOn() {
        System.out.println("Device Turned On");
    }

    @Override
    public void turnOff() {
        System.out.println("Device Turned Off");

    }

    @Override
    public void pauze() {
        System.out.println("Song pauzed");

    }

    @Override
    public void play() {
        System.out.println("Play Song");

    }
    @Override
    public void lowerVolume() {
        volume -= 1;
        System.out.println("Volume is " + volume);
    }

    @Override
    public void higherVolume() {
        volume += 1;
        System.out.println("Volume is " + volume);

    }
}
