package nl.bioinf.design_patterns.command.radioexample;

public class TurnRadioOn implements Command {

    ElectronicDevice radioDevice;

    public TurnRadioOn(ElectronicDevice usedDevice) {
        radioDevice = usedDevice;
    }
    @Override
    public void execute() {
        radioDevice.turnOn();
    }

    @Override
    public void undo() {
        radioDevice.turnOff();
    }
}
