package nl.bioinf.design_patterns.command.tvexample;

public class TVRemote {

    public static ElectronicDevice getDevice() {

        return new Television();
    }
}
