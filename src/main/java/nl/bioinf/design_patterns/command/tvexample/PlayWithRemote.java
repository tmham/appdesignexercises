package nl.bioinf.design_patterns.command.tvexample;

public class PlayWithRemote {

    public static void main(String[] args) {

        ElectronicDevice newDevice = TVRemote.getDevice();
        TurnTVOn onCommand = new TurnTVOn(newDevice);
        DeviceButton onPressed = new DeviceButton(onCommand);
        onPressed.press();

        TurnTVUp volumeUpCommand = new TurnTVUp(newDevice);
        onPressed = new DeviceButton(volumeUpCommand);
        onPressed.press();
        onPressed.press();

    }
}
