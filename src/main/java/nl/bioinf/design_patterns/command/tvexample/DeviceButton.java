package nl.bioinf.design_patterns.command.tvexample;

public class DeviceButton {

    Command theCommand;
    public DeviceButton(Command newCommand) {

        theCommand = newCommand;
    }

    public void press(){
        theCommand.execute();
    }
}
