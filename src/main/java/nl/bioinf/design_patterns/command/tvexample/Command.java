package nl.bioinf.design_patterns.command.tvexample;

public interface Command {
    public void execute();
    public void undo();
}
