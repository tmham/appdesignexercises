package nl.bioinf.design_patterns.filter;

import java.util.ArrayList;
import java.util.List;

public class CriteriaSingle implements Criteria {
    List<Person> singlePersons = new ArrayList<Person>();

    @Override
    public List<Person> meetCriteria(List<Person> persons) {
        for (Person person : persons) {
            if (person.getMaritalStatus().equals("SINGLE")) {
                singlePersons.add(person);
            }
        }
        return singlePersons;
    }
}
