package nl.bioinf.design_patterns.filter;

import java.util.ArrayList;
import java.util.List;

public class OrCriteria implements Criteria {
    private Criteria criteria;
    private Criteria otherCriteria;

    public OrCriteria(Criteria criteria, Criteria otherCriteria) {
        this.criteria = criteria;
        this.otherCriteria = otherCriteria;
    }

    @Override
    public List<Person> meetCriteria(List<Person> persons) {
        List<Person> firstCriteriaMet = criteria.meetCriteria(persons);
        List<Person> otherCriteriaMet = otherCriteria.meetCriteria(persons);
        for (Person person : otherCriteriaMet) {
            if (!firstCriteriaMet.contains(person)) {
                firstCriteriaMet.add(person);
            }
        }
        return firstCriteriaMet;
    }
}
