package nl.bioinf.design_patterns.builder;

public class TestBankAccount {
    public static void main(String[] args) {
        BankAccount bankAccount = new BankAccount.Builder(1234)
                .setOwner("HENK")
                .setBalance(1234)
                .setBranch("GRONINGEN")
                .setInterestRate(0.5)
                .build();
        System.out.println(bankAccount.getAccountNumber());
    }

}
