package nl.bioinf.design_patterns.builder;

public class Address {
    // declare finals
    private final String streetname;
    private final String zipcode;
    private final int houseNumber;

    private Address(AddressBuilder builder) {
        this.streetname = builder.streetname;
        this.zipcode = builder.zipcode;
        this.houseNumber = builder.houseNumber;
    }

    public static class AddressBuilder {
        private final String streetname;
        private final int houseNumber;
        private String zipcode;

        public AddressBuilder(String streetname, int houseNumber) {
            this.streetname = streetname;
            this.houseNumber = houseNumber;
        }

        public AddressBuilder setZipcode(String zipcode) {
            this.zipcode = zipcode;
            return this;
        }

        public Address build() {
            Address address = new Address(this);
            return address;
        }
    }

    public String getStreetname() {
        return streetname;
    }

    public String getZipcode() {
        return zipcode;
    }

    public int getHouseNumber() {
        return houseNumber;
    }
}

