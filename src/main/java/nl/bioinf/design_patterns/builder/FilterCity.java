package nl.bioinf.design_patterns.builder;

import java.util.ArrayList;
import java.util.List;

public class FilterCity implements filter {

    List<List<Address>> cities = new ArrayList<>();
    List<Address> citySchijndel= new ArrayList<>();
    List<Address> cityGroningen = new ArrayList<>();

    @Override
    public List<List<Address>> filterAddress(List<Address> toBeFiltered) {
        for (Address address : toBeFiltered) {
            System.out.println(address.getCity());
            if (address.getCity() == "SCHIJNDEL") {
                citySchijndel.add(address);
            }
            if (address.getCity() == "GRONINGEN") {
                cityGroningen.add(address);
            }
        }
        cities.add(citySchijndel);
        cities.add(cityGroningen);
        return cities;
    }
}
