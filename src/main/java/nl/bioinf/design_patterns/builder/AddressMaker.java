package nl.bioinf.design_patterns.builder;

import java.util.ArrayList;
import java.util.List;

public class AddressMaker {
    public static void main(String[] args) {
        int addressesInGroningenIndex = 2;
        int addressesInSchijndelIndex = 1;
        Address addressGroningen = new Address.AddressBuilder("Gorechtkade", "9713BE", 39)
                .setCity("GRONINGEN")
                .setCountry("NEDERLAND")
                .setNeighourhood("EUSTAPARK")
                .build();

        Address addressSchijndel = new Address.AddressBuilder("Putsteeg", "5481XT", 91)
                .setCity("SCHIJNDEL")
                .setCountry("NEDERLAND")
                .build();

        List<Address> addressList = new ArrayList<>();
        addressList.add(addressGroningen);
        addressList.add(addressSchijndel);


        FilterCity cityFilter = new FilterCity();
        List<List<Address>> addressessFilterByCity  = cityFilter.filterAddress(addressList);

        HouseNumberFilter houseNumberFilter = new HouseNumberFilter();
        List<List<Address>> addressessFilterByHouseNumber = houseNumberFilter.filterAddress(addressList);
        for (List<Address> addresses : addressessFilterByCity)  {
            List<Address> firstHouseNumberList = addresses;
            for (Address address : firstHouseNumberList) {
                System.out.println(address.getHouseNumber());
            }
        }
    }


}
