package nl.bioinf.design_patterns.builder;

import java.util.List;

public interface filter {
    List<List<Address>> filterAddress(List<Address> toBeFiltered);
}
