package nl.bioinf.design_patterns.builder;

import java.util.ArrayList;
import java.util.List;

public class HouseNumberFilter implements filter {
    List<Address> houseNumberAbove50 = new ArrayList<>();
    List<Address> houseNumberBelow50 = new ArrayList<>();

    List<List<Address>> filterByHouseNumber = new ArrayList<>();


    @Override
    public List<List<Address>> filterAddress(List<Address> toBeFiltered) {
        for (Address address : toBeFiltered) {
            if (address.getHouseNumber() >= 50) {
                houseNumberAbove50.add(address);
            } else {
                houseNumberBelow50.add(address);
            }

        }
        filterByHouseNumber.add(houseNumberAbove50);
        filterByHouseNumber.add(houseNumberBelow50);
        return filterByHouseNumber;
    }
}
