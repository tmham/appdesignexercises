package nl.bioinf.design_patterns.observer.stock;

public interface Subject {
    public void register(Observer o);
    public void unregister(Observer o);
    public void notifyObserver();

}
