package nl.bioinf.design_patterns.observer.stock;

public interface Observer {
    public void update(double ibmPrice, double aaplPrice, double googPrice);
}
