package nl.bioinf.design_patterns.observer.stock;

public class GrabStocks {
    public static void main(String[] args) {
        StockGrabber stockGrabber = new StockGrabber();

        StockObserver observer1 = new StockObserver(stockGrabber);

        stockGrabber.setAaplPrice(677.60);
        stockGrabber.setIbmPrice(197.00);
        stockGrabber.setGoogPrice(676.40);
    }
}
