package nl.bioinf.design_patterns.observer.sensordata;

public interface Observer {
    void update(SensorData sensorData);
}
