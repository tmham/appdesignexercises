package nl.bioinf.design_patterns.observer.sensordata;

public class SensorLogger implements Observer {
    @Override
    public void update(SensorData sensorData) {
        System.out.println("Logging Sensor Data " + sensorData.getSensor() + " " + sensorData.getMeasure());
    }
}
