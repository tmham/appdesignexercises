package nl.bioinf.design_patterns.observer.sensordata;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SensorReceiver implements Observable {

    private List<SensorData> data = new ArrayList<>();
    private List<Observer> observers = new ArrayList<>();


    @Override
    public void register(Observer ob) {
        observers.add(ob);

    }

    @Override
    public void unregister(Observer ob) {
        observers.remove(ob);

    }

    public void addData(SensorData date) {
        data.add(date);
    }

    @Override
    public void updateObservers() {
        Iterator iterator = data.iterator();
        while (iterator.hasNext()) {

            SensorData sensorData = (SensorData) iterator.next();

            for (Observer observer : observers) {
                observer.update(sensorData);
            }

            iterator.remove();
        }
    }

}
