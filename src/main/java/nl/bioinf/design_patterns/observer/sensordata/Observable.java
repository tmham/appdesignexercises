package nl.bioinf.design_patterns.observer.sensordata;

public interface Observable {
    void register(Observer ob);

    void unregister(Observer ob);

    void updateObservers();
}
