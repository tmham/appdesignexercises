package nl.bioinf.singleton;

public class SingletonTester {
    public static void main(String[] args) {
        for (int i = 0; i < 100000; i++) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    final ClassicSingleton instance = ClassicSingleton.getInstance();
                    if (ClassicSingleton.instanceCount != 1) {
                        System.out.println("instance = " + instance);
                    }
                }
            });
            t.start();
        }
        System.out.println("instance = " + ClassicSingleton.instanceCount);
    }
}
