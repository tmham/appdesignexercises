package nl.bioinf.singleton;

public class ClassicSingleton {
    static int instanceCount = 0;

    private static ClassicSingleton instance = new ClassicSingleton();
    /**
     * private constructor ensures no one can
     * instantiate it beside its own class!
     */
    private ClassicSingleton() {
        instanceCount++;
    }
    /**
     * The only means to get hold of the instance.
     * Uses lazy instantiation.
     * @return
     */
    public static ClassicSingleton getInstance() {
//        if (instance == null)
//            instance = new ClassicSingleton();
        return instance;
    }

    public String toString() {
        return "instanceCount = " + instanceCount;
    }
}
