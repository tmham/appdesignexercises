package nl.bioinf.birds;

public class NoFlyBehavior implements FlyBehavior {
    @Override
    public void fly() throws NoFlightException {
        throw new NoFlightException("No way I am flying");
    }
}
