package nl.bioinf.birds;

public class SoaringFlyBehavior implements FlyBehavior{
    @Override
    public void fly() throws NoFlightException {
        System.out.println("soaring on thermal vents");
    }
}
