package nl.bioinf.birds;

public abstract class Bird {
    private FlyBehavior flyBehavior;

    public void setFlyBehaviour(FlyBehavior flyBehavior) {
        this.flyBehavior = flyBehavior;
    }

    public void fly() {
        this.flyBehavior.fly();
    }

//    public abstract void buildNest();
}
