package nl.bioinf.birds;

public interface FlyBehavior {
    void fly () throws NoFlightException;
}
