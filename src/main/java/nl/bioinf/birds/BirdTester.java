package nl.bioinf.birds;

public class BirdTester {
    public static void main(String[] args) {
        FlyBehavior speedster = new SpeedsterFlyBehavior();
        FlyBehavior noFly = new NoFlyBehavior();
        FlyBehavior soaring = new SoaringFlyBehavior();

        Bird buzzard = new Buzzard();
        buzzard.setFlyBehaviour(soaring);
        buzzard.fly();

        Bird puffin = new Puffin();
        puffin.setFlyBehaviour(speedster);
        puffin.fly();

        Bird kiwi = new Kiwi();
        kiwi.setFlyBehaviour(noFly);
        try{
            kiwi.fly();
        } catch (NoFlightException ex) {
            System.out.println(ex.getMessage());
        }


//        Bird bird = new Bird()
    }
}
