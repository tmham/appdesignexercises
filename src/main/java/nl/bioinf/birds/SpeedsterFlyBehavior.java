package nl.bioinf.birds;

public class SpeedsterFlyBehavior implements FlyBehavior {
    @Override
    public void fly() throws NoFlightException {
        System.out.println("flapping my wings at incredible frequency!");
    }
}
