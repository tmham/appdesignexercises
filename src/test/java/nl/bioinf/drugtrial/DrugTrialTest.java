package nl.bioinf.drugtrial;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DrugTrialTest {
    DrugTrial drugTrial;

    @BeforeEach
    void setupDrugTrial() {
        drugTrial = new DrugTrial();
    }

    @ParameterizedTest
    @ValueSource(strings = {"ACV5X", "JAG4S"})
    void addSubjectSunny(String id) {
        Subject subject = new Subject(id);
        drugTrial.addSubject(subject);
        List<Subject> subjects = drugTrial.getSubjects();
        assertEquals(1, subjects.size());

        assertEquals(id, subjects.get(0).getId());
    }

    @Test
    void addSubjectNull() {
        assertThrows(IllegalArgumentException.class,
                () -> drugTrial.addSubject(null),
                "should have thrown an exception");

        //equal to
        try {
            drugTrial.addSubject(null);
            fail("should have thrown an exception");
        } catch (IllegalArgumentException ex) {
            //OK!
        }
    }

    @Test
    void performTrial() {
        Subject subject = new Subject("FOO");
        drugTrial.addSubject(subject);
        drugTrial.addSubject(subject);
        drugTrial.addSubject(subject);
        drugTrial.addSubject(subject);
        drugTrial.addSubject(subject);
        drugTrial.performTrial();
    }
}